export class Table {
  public static tableTransactions = [
    {
      name: 'Fecha de transporte',
      width: '80',
    },
    {
      name: 'Código de la reserva',
      width: '135',
    },
    {
      name: 'Número de documento',
      width: '100',
    },
    {
      name: 'Nombres y apellidos cliente',
      width: '170',
    },
    {
      name: 'Empresa transportadora',
      width: '110',
    },
    {
      name: 'Punto de recogida',
      width: '200',
    },
    {
      name: 'Nombre del conductor',
      width: '200',
    },
    {
      name: 'Número de documento del conductor',
      width: '120',
    }
  ];

  public static tableSettlement = [
    {
      name: 'Fecha de transporte',
      width: '55',
    },
    {
      name: 'Código de la reserva',
      width: '200',
    },
    {
      name: 'Número de documento',
      width: '200',
    },
    {
      name: 'Nombres y apellidos cliente',
      width: '55',
    },
    {
      name: 'Empresa transportadora',
      width: '267',
    },
    {
      name: 'Punto de recogida',
      width: '150',
    },
    {
      name: 'Nombre del conductor',
      width: '200',
    },
    {
      name: 'Fecha y hora de ida',
      width: '55',
    },
    {
      name: 'Fecha y hora de regreso',
      width: '267',
    },
    {
      name: 'Número de doc. del conductor',
      width: '150',
    }
  ];

  public static tableDemand = [
    {
      name: 'Fecha de transporte',
      width: '55',
    },
    {
      name: 'Punto de recogida',
      width: '200',
    },
    {
      name: 'Total de personas',
      width: '150',
    }
  ];

  public static tableCompanies = [
    {
      name: '',
      width: '55',
    },
    {
      name: 'Nombre de empresa transportadora',
      width: '267',
    },
    {
      name: 'NIT',
      width: '150',
    },
    {
      name: 'Correo electrónico',
      width: '400',
    }
  ];

  public static tablePoints = [
    {
      name: '',
      width: '55',
    },
    {
      name: 'Punto de recogida',
      width: '134',
    },
    {
      name: 'Dirección',
      width: '170',
    },
    {
      name: 'Ubicación para mapas',
      width: '134',
    },
    {
      name: 'Teléfono de contacto',
      width: '134',
    },
    {
      name: 'Empresa transportadora',
      width: '170',
    },
    {
      name: 'Horario de salida',
      width: '130',
    },
    {
      name: 'Recomendaciones',
      width: '400',
    }
  ];

  public static tableUsers = [
    {
      name: '',
      width: '55',
    },
    {
      name: 'Nombres y Apellidos',
      width: '300',
    },
    {
      name: 'Número de documento',
      width: '150',
    }
  ];
}
