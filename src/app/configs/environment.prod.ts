export const environment = {
  production: true,
    url: 'https://transacciones.colsubsidio.com/portalempresas/admin/preafiliacionesbackend/',
    URL_REPORTES: 'https://transacciones.colsubsidio.com/portalempresas/admin/reportes',
    maxsize: 30000000
};
