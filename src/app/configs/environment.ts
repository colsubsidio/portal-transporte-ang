// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  loginUserToken: 'appLoginUserToken',
  dataConsultPlan : 'dataConsultPlan',
  END_POINT: 'http://40.117.63.112/rtur/portal/transporte/admin/v1.0',
  END_POINT_REPORT: 'http://40.117.63.112/rtur/portal/transporte/reportes/v1.0/informes',
  END_POINT_MOCK: 'https://run.mocky.io/v3/2b05d0f5-d7f0-4ce0-b77d-b78a444ab7a0',
  URL_USER_AUTH : 'http://40.117.63.112/rtur/portal/transporte/auth/v1.0',
  TRANSPORTATION_USERS : '/users',
  maxsize: 30000000,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
