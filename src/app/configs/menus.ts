import { ERole } from '../core/models/erole';

export class Menus {
  public static menuItems = [
    {
      name: 'Transacciones',
      icon: 'list',
      path: '/administrador-transporte/transacciones',
      nameEvent: '',
      roles: [ERole.administrator],
      childrens : null
    },
    {
      name: 'Liquidación',
      icon: 'list',
      path: '/administrador-transporte/liquidacion',
      nameEvent: '',
      roles: [ERole.company, ERole.administrator],
      childrens : null
    },
    {
      name: 'Demanda',
      icon: 'list',
      path: '/administrador-transporte/demanda',
      nameEvent: '',
      roles: [ERole.company, ERole.administrator],
      childrens : null
    },
    {
      name: 'Puntos de transporte',
      icon: 'list',
      path: '/administrador-transporte/puntos-transporte',
      nameEvent: '',
      roles: [ERole.administrator],
      childrens : null
    },
    {
      name: 'Transportadores',
      icon: 'list',
      path: '/administrador-transporte/transportadores',
      nameEvent: '',
      roles: [ERole.company],
      childrens : null
    },
    {
      name: 'Empresas',
      icon: 'list',
      path: '/administrador-transporte/empresas',
      nameEvent: '',
      roles: [ERole.administrator],
      childrens : null
    }
  ];
}
