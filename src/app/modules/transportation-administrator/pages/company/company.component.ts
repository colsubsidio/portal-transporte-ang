import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms'
import { RegularExpressions } from 'src/app/core/helpers/regular-expressions/regular-expressions';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  company: FormGroup;
  textTitle= 'Agregar empresa transportadora';
  textButton = 'Agregar empresa';
  message: string = '';
  showMessage: string;

  constructor(private fb: FormBuilder,
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.company = this.fb.group({
      nit: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_OTROS)]],
      name: ['', [Validators.required, Validators.pattern(RegularExpressions.LETTERS_SPACES_SPECIALS)]],
      email: ['', [Validators.required, Validators.email, Validators.pattern(RegularExpressions.VALIDATE_EMAIL)]]
    });
  }

  public onSubmit() {
    let data = {
      correo: this.company.get('email').value,
      nit: this.company.get('nit').value,
      nombreEmpresa: this.company.get('name').value,
    }

    this.transportationService.createCompany(data).subscribe(
      response => {
        if (response.resultado[0].codigo == 200) {
          this.showMessage = 'success'
          this.message = response.resultado[0].descripcion

          setTimeout(() => {
            this.return();
          }, 2000);


        } else {
          this.showMessage = 'error'
          this.message = response.resultado[0].descripcion
        }
      }
    );
  }

  public return() {
    this.router.navigate(['/administrador-transporte/empresas'], {
      relativeTo: this.route
    });
  }

  closeError() {
    this.message = '';
  }
}
