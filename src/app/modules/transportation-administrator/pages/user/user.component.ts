import { RegularExpressions } from './../../../../core/helpers/regular-expressions/regular-expressions';
import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {
  user: FormGroup;
  tokenInfo: any;
  companies: Companies[] = [];
  message: string = '';
  showMessage: string;

  textTitle= 'Agregar usuario transportador';
  textButton = 'Agregar usuario';

  constructor(
    private fb: FormBuilder,
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.user = this.fb.group({
      names: ['', [Validators.required, Validators.pattern(RegularExpressions.LETTERS_SPACES_SPECIALS)]],
      documentNumber: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_NUMBERS)]]
    });

    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  onSubmit() {
    let data = {
      nitEmpresa: this.tokenInfo.numDoc,
      nombreCompleto: this.user.get('names').value,
      numeroDocumento: this.user.get('documentNumber').value,
    }

    this.transportationService.createTransporter(data).subscribe(
      response => {
        if (response.resultado[0].codigo == 200) {
          this.showMessage = 'success'
          this.message = response.resultado[0].descripcion

          setTimeout(() => {
            this.return();
          }, 2000);


        } else {
          this.showMessage = 'error'
          this.message = response.resultado[0].descripcion
        }
      }
    );
  }

  public return() {
    this.router.navigate(['/administrador-transporte/transportadores'], {
      relativeTo: this.route
    });
  }

  closeError() {
    this.message = '';
  }
}
