import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import jwt_decode from "jwt-decode";
import { Transporter } from 'src/app/core/models/transporter';
import { RegularExpressions } from 'src/app/core/helpers/regular-expressions/regular-expressions';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserUpdateComponent implements OnInit {
  user: FormGroup;
  companies: Companies[] = [];
  tokenInfo: any;
  param: any;
  transporterGet: Transporter;
  message: string = '';
  showMessage: string;

  textTitle = 'Modificar información del usuario';
  textButton = 'Modificar';

  constructor(
    private fb: FormBuilder,
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.param = this.route.snapshot.paramMap.get('id');

    this.user = this.fb.group({
      names: ['', [Validators.required, Validators.pattern(RegularExpressions.LETTERS_SPACES_SPECIALS)]],
      documentNumber: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_NUMBERS)]]
    });

    this.getTransporter();
    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  getTransporter() {
    this.transportationService.getTransporter(this.param).subscribe(
      res => {
        this.transporterGet = res.data;

        if(this.transporterGet !== null) {
          this.loadData();
        }
      }
    );
  }

  loadData() {
    this.user.get('names').setValue(this.transporterGet.nombreCompleto);
    this.user.get('documentNumber').setValue(this.transporterGet.numeroDocumento);
  }

  onSubmit() {
    let data = {
      nitEmpresa: this.tokenInfo.numDoc,
      nombreCompleto: this.user.get('names').value,
      numeroDocumento: this.user.get('documentNumber').value,
      partitionKey: this.transporterGet.partitionKey,
      rowKey: this.transporterGet.rowKey
    }

    this.transportationService.updateTransporter(data).subscribe(
      response => {
        if (response.resultado[0].codigo == 200) {
          this.showMessage = 'success'
          this.message = response.resultado[0].descripcion

          setTimeout(() => {
            this.return();
          }, 2000);


        } else {
          this.showMessage = 'error'
          this.message = response.resultado[0].descripcion
        }
      }
    );
  }

  public return() {
    this.router.navigate(['/administrador-transporte/transportadores'], {
      relativeTo: this.route
    });
  }

  closeError() {
    this.message = '';
  }
}
