import { Table } from '../../../../configs/table';
import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CompaniesComponent implements OnInit {

  showDes: boolean = false;
  companies: Companies[] = [];
  headTable = Table.tableCompanies;

  constructor(
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute,
    private _CHANGEDECTOR: ChangeDetectorRef,) { }

  ngOnInit(): void {
    this.getCompanies();
    this._CHANGEDECTOR.markForCheck();
  }

  getCompanies() {
    this.transportationService.getCompanies().subscribe(
      res => {
        this.companies = res.data;
        this._CHANGEDECTOR.markForCheck();
      }
    );
  }

  despliegue() {
    this.showDes = !this.showDes;
  }

  edit(key) {
    this.router.navigate(['/administrador-transporte/empresa', key], {
      relativeTo: this.route
    });
  }

  disabledCompany(key) {
    this.transportationService.deleteCompany(key).subscribe(
      res => {
        this.getCompanies();
      }
    );

    this.clearRadio(key)
  }

  clearRadio(id) {
    const idInputaa = document.getElementById(id) as HTMLInputElement;

    if(idInputaa.checked == true) {
      idInputaa.checked = false;
    }
  }

}
