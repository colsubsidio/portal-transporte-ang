import { Token } from './../../../../core/models/token';
import { TransportationService } from './../../../../core/services/trabsportation/transportation.service';
import { ReportsService } from './../../../../core/services/reports/reports.service';
import { Component, OnInit } from '@angular/core';
import { Table } from 'src/app/configs/table';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Points } from 'src/app/core/models/points';
import * as moment from 'moment';
import jwt_decode from 'jwt-decode';
import { Demand } from 'src/app/core/models/demand';
import { environment } from 'src/app/configs/environment';
import { ERole } from 'src/app/core/models/erole';

@Component({
  selector: 'app-demand',
  templateUrl: './demand.component.html',
  styleUrls: ['./demand.component.scss']
})
export class DemandComponent implements OnInit {
  showDes = false;
  listPoints: Points[];
  listDemand: Demand[];
  headTable = Table.tableDemand;
  tokenInfo: Token;

  public dataForm: FormGroup;
  public closeFil: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private reportsService: ReportsService,
    private transportationService: TransportationService
  ) {
    this.dataForm = this._formBuilder.group({
      transporter: [''],
      date: ['']
    });
  }

  ngOnInit(): void {
    this.closeFil = false;
    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));
    this.getDemand();
    this.validatePoints();
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  closeFilter(value: string) {
    if (value === 'abrir') {
      this.closeFil = true;
    } else {
      this.closeFil = false;
    }
  }

  validatePoints() {
    if ( this.tokenInfo.authorities[0] === ERole.company ) {
      this.getPointsForBusiness();
    } else {
      this.getPoints();
    }
  }

  getNit() {
    if ( this.tokenInfo.authorities[0] === ERole.company ) {
      return this.tokenInfo.numDoc;
    } else {
      return '';
    }
  }

  async getPointsForBusiness() {
    this.listPoints = await this.transportationService.getPointsForBusiness(this.tokenInfo.numDoc).toPromise();
  }

  async getPoints() {
    const points = await this.transportationService.getPoints().toPromise();
    this.listPoints = points.data;
  }

  async getDemand() {
    debugger
    const transactionDate = (this.dataForm.value.date) ? (moment(this.dataForm.value.date).add(moment.duration('05:00:00'))).format('YYYY-MM-DD') : '';
    this.listDemand = await this.reportsService.getDemand(
      this.getNit(),
      this.dataForm.value.transporter,
      transactionDate).toPromise();
  }


  continue() {
    this.closeFilter('cerrar');
    this.getDemand();
  }

}
