import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportationPointsComponent } from './transportation-points.component';

describe('TransportationPointsComponent', () => {
  let component: TransportationPointsComponent;
  let fixture: ComponentFixture<TransportationPointsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportationPointsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportationPointsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
