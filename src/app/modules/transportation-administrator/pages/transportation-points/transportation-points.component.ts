import { Points } from '../../../../core/models/points';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Table } from 'src/app/configs/table';

@Component({
  selector: 'app-transportation-points',
  templateUrl: './transportation-points.component.html',
  styleUrls: ['./transportation-points.component.scss']
})
export class TransportationPointsComponent implements OnInit {
  showDes = false;
  points: Points[] = [];
  headTable = Table.tablePoints;

  constructor(
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getPoints();
  }

  getPoints() {
    this.transportationService.getPoints().subscribe(
      res => {
        this.points = res.data;
      }
    );
  }

  despliegue() {
    this.showDes = !this.showDes;
  }

  edit(key) {
    this.router.navigate(['/administrador-transporte/punto-transporte', key], {
      relativeTo: this.route
    });
  }

  deletePoint(key) {
    this.transportationService.deletePoint(key).subscribe(
      res => {
        this.getPoints();
      }
    );
  }

}
