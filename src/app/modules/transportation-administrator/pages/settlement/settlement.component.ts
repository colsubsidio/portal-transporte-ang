import { TransportationService } from './../../../../core/services/trabsportation/transportation.service';
import { Points } from './../../../../core/models/points';
import jwt_decode from 'jwt-decode';
import { ReportsService } from './../../../../core/services/reports/reports.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { Table } from 'src/app/configs/table';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { Settlement, SettlementCsv } from 'src/app/core/models/settlement';
import { ExportToCsv } from 'export-to-csv';
import { ERole } from 'src/app/core/models/erole';


@Component({
  selector: 'app-settlement',
  templateUrl: './settlement.component.html',
  styleUrls: ['./settlement.component.scss']
})
export class SettlementComponent implements OnInit {
  showDes = false;
  listSettlement: Settlement[];
  listPoints: Points[];
  tokenInfo: any;
  private listSettlementCsv: SettlementCsv[] = new Array<SettlementCsv>() ;
  public dataForm: FormGroup;
  public closeFil: boolean;
  public moment = moment;

  headTable = Table.tableSettlement;

  constructor(
    private _formBuilder: FormBuilder,
    private reportsService: ReportsService,
    private transportationService: TransportationService
  ) {
    this.dataForm = this._formBuilder.group({
      point: [''],
      since: [''],
      until: ['']
    });
  }

  ngOnInit(): void {
    this.closeFil = false;
    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));
    this.getSettlement();
    this.validatePoints();
  }

  validatePoints() {
    if ( this.tokenInfo.authorities[0] === ERole.company ) {
      this.getPointsForBusiness();
    } else {
      this.getPoints();
    }
  }

  closeFilter(value: string) {
    if (value === 'abrir') {
      this.closeFil = true;
    } else {
      this.closeFil = false;
    }
  }

  continue() {
    this.closeFilter('cerrar');
    this.getSettlement();
  }

  async getSettlement() {
    const initDate = (this.dataForm.value.since) ? (moment(this.dataForm.value.since)).format('YYYY-MM-DD') : '';
    const lastDate = (this.dataForm.value.until) ? (moment(this.dataForm.value.until)).format('YYYY-MM-DD') : '';

    this.listSettlement = await this.reportsService.getSettlement(
      this.getNit(),
      this.dataForm.value.point,
      initDate,
      lastDate).toPromise();
  }

  getNit() {
    if ( this.tokenInfo.authorities[0] === ERole.company ) {
      return this.tokenInfo.numDoc;
    } else {
      return '';
    }
  }

  async getPointsForBusiness() {
    this.listPoints = await this.transportationService.getPointsForBusiness(this.tokenInfo.numDoc).toPromise();
  }

  async getPoints() {
    const points = await this.transportationService.getPoints().toPromise();
    this.listPoints = points.data;
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  exportCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: false,
      title: '',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true
    };

    this.listSettlement.forEach(settlement => {
      let nameClient = (settlement.cliente.primerNombre) ? settlement.cliente.primerNombre : '';
      nameClient = (settlement.cliente.segundoNombre) ? nameClient + ' ' + settlement.cliente.segundoNombre : nameClient;
      nameClient = (settlement.cliente.primerApellido) ? nameClient + ' ' + settlement.cliente.primerApellido : nameClient;
      nameClient = (settlement.cliente.segundoApellido) ? nameClient + ' ' + settlement.cliente.segundoApellido : nameClient;

      const settlementCsv: SettlementCsv = new SettlementCsv();
      settlementCsv.empresaTransportadora = settlement.reserva?.transporte?.transportadora?.nombreCompleto;
      settlementCsv.fechaHoraIda = settlement.reserva?.transporte?.fechaIda;
      settlementCsv.fechaHoraRegreso = settlement.reserva?.transporte?.fechaRegreso;
      settlementCsv.fechaTransporte = settlement.reserva?.transporte?.fechaReserva;
      settlementCsv.nombreCompletoCliente = nameClient;
      settlementCsv.nombreConductor = settlement.reserva?.transporte?.transportador?.nombreCompleto;
      settlementCsv.numeroDoc = settlement.cliente?.numDoc;
      settlementCsv.numeroDocConductor = settlement.reserva?.transporte?.transportador?.numDoc;
      settlementCsv.puntoRecogida = settlement.reserva?.transporte?.puntoRecogida;
      settlementCsv.tipoDoc = '';
      settlementCsv.codigoReserva = settlement.reserva?.codigo;

      this.listSettlementCsv.push(settlementCsv);
    });

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.listSettlementCsv);
  }

}
