import { Transporters } from '../../../../core/models/transporters';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Table } from 'src/app/configs/table';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-transporters',
  templateUrl: './transporters.component.html',
  styleUrls: ['./transporters.component.scss']
})
export class TransportersComponent implements OnInit {
  showDes: boolean = false;
  transporters: Transporters[] = [];
  headTable = Table.tableUsers;
  tokenInfo: any;

  constructor(
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));

    if(this.tokenInfo !== '') {
      this.getUsers(this.tokenInfo.numDoc);
    }
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  getUsers(nit) {
    this.transportationService.getTransporters(nit).subscribe(
      res => {
        this.transporters = res.data;
      }
    );
  }

  despliegue() {
    this.showDes = !this.showDes;
  }

  edit(key) {
    this.router.navigate(['/administrador-transporte/transportador', key], {
      relativeTo: this.route
    });
  }

  deleteTransporter(key) {
    this.transportationService.deleteTransporter(key).subscribe(
      res => {
        this.getUsers(this.tokenInfo.numDoc);
      }
    );
  }
}
