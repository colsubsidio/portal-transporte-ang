import { ExportToCsv } from 'export-to-csv';
import { TransactionsCsv } from './../../../../core/models/settlement';
import { Companies } from './../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { Component, OnInit } from '@angular/core';
import { Table } from 'src/app/configs/table';
import * as moment from 'moment';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Settlement } from 'src/app/core/models/settlement';
import { ReportsService } from 'src/app/core/services/reports/reports.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  showDes = false;
  listBusiness: Companies[];
  listTransaction: Settlement[];
  private listTransactionsCsv: TransactionsCsv[] = new Array<TransactionsCsv>() ;
  public dataForm: FormGroup;
  public closeFil: boolean;

  headTable = Table.tableTransactions;

  constructor(
    private _formBuilder: FormBuilder,
    private reportsService: ReportsService,
    private transportationService: TransportationService
  ) {
    this.dataForm = this._formBuilder.group({
      transporter: [''],
      since: [''],
      until: ['']
    });
  }

  ngOnInit(): void {
    this.closeFil = false;
    this.getBusiness();
    this.getTransactions();
  }

  async getBusiness() {
    const responseBusines: any = await this.transportationService.getCompanies().toPromise();
    this.listBusiness = responseBusines.data;
  }

  async getTransactions() {
    const initDate = (this.dataForm.value.since) ? (moment(this.dataForm.value.since)).format('YYYY-MM-DD') : '';
    const lastDate = (this.dataForm.value.until) ? (moment(this.dataForm.value.until)).format('YYYY-MM-DD') : '';

    this.listTransaction = await this.reportsService.getTransaction(
      this.dataForm.value.transporter,
      initDate,
      lastDate).toPromise();
  }

  closeFilter(value: string) {
    if (value === 'abrir') {
      this.closeFil = true;
    } else {
      this.closeFil = false;
    }
  }

  continue() {
    this.closeFilter('cerrar');
    this.getTransactions();
  }

  exportCsv() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '',
      decimalSeparator: '.',
      showLabels: true,
      showTitle: false,
      title: '',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true
    };

    this.listTransaction.forEach(transaction => {
      let nameClient = (transaction.cliente.primerNombre) ? transaction.cliente.primerNombre : '';
      nameClient = (transaction.cliente.segundoNombre) ? nameClient + ' ' + transaction.cliente.segundoNombre : nameClient;
      nameClient = (transaction.cliente.primerApellido) ? nameClient + ' ' + transaction.cliente.primerApellido : nameClient;
      nameClient = (transaction.cliente.segundoApellido) ? nameClient + ' ' + transaction.cliente.segundoApellido : nameClient;

      const transactionsCsv: TransactionsCsv = new TransactionsCsv();
      transactionsCsv.empresaTransportadora = transaction.reserva?.transporte?.transportadora?.nombreCompleto;
      transactionsCsv.fechaTransporte = transaction.reserva?.transporte?.fechaReserva;
      transactionsCsv.nombreCompletoCliente = nameClient;
      transactionsCsv.nombreConductor = transaction.reserva?.transporte?.transportador?.nombreCompleto;
      transactionsCsv.numeroDoc = transaction.cliente?.numDoc;
      transactionsCsv.numeroDocConductor = transaction.reserva?.transporte?.transportador?.numDoc;
      transactionsCsv.puntoRecogida = transaction.reserva?.transporte?.puntoRecogida;
      transactionsCsv.tipoDoc = '';
      transactionsCsv.codigoReserva = transaction.reserva?.codigo;

      this.listTransactionsCsv.push(transactionsCsv);
    });

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(this.listTransactionsCsv);
  }

}
