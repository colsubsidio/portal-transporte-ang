import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { RegularExpressions } from 'src/app/core/helpers/regular-expressions/regular-expressions';

@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss']
})
export class PointComponent implements OnInit {

  point: FormGroup;
  companies: Companies[] = [];

  textTitle = 'Agregar punto de transporte';
  textButton = 'Agregar punto';
  message: string = '';
  showMessage: string;

  constructor(private fb: FormBuilder,
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.point = this.fb.group({
      name: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_OTROS)]],
      address: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_OTROS)]],
      location: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_NUMBERS)]],
      company: ['', [Validators.required]],
      exit: ['', [Validators.required]],
      recommendations: ['', [Validators.required, Validators.pattern(RegularExpressions.LETTERS_SPACES_SPECIALS)]]
    });

    this.getCompanies();
  }

  getCompanies() {
    this.transportationService.getCompanies().subscribe(
      res => {
        this.companies = res.data;
      }
    );
  }

  onSubmit() {
    let data = {
      direccion: this.point.get('address').value,
      horarioSalida: this.point.get('exit').value,
      nitEmpresa: this.point.get('company').value,
      nombrePunto: this.point.get('name').value,
      recomendaciones: this.point.get('recommendations').value,
      telefono: this.point.get('phone').value,
      ubicacionMapa: this.point.get('location').value
    }

    this.transportationService.createPoint(data).subscribe(
      response => {
        if (response.resultado[0].codigo == 200) {
          this.showMessage = 'success'
          this.message = response.resultado[0].descripcion

          setTimeout(() => {
            this.return();
          }, 2000);


        } else {
          this.showMessage = 'error'
          this.message = response.resultado[0].descripcion
        }
      }
    );
  }

  public return() {
    this.router.navigate(['/administrador-transporte/puntos-transporte'], {
      relativeTo: this.route
    });
  }

  closeError() {
    this.message = '';
  }

}
