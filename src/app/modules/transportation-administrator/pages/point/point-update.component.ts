import { Companies } from '../../../../core/models/companies';
import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Point } from 'src/app/core/models/point';
import { RegularExpressions } from 'src/app/core/helpers/regular-expressions/regular-expressions';

@Component({
  selector: 'app-point',
  templateUrl: './point.component.html',
  styleUrls: ['./point.component.scss']
})
export class PointUpdateComponent implements OnInit {

  point: FormGroup;
  param: any;
  pointGet: Point;
  companies: Companies[] = [];
  message: string = '';
  showMessage: string;

  textTitle = 'Modificar información del punto de transporte';
  textButton = 'Modificar';

  constructor(private fb: FormBuilder,
    private transportationService: TransportationService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.param = this.route.snapshot.paramMap.get('id');

    this.point = this.fb.group({
      name: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_OTROS)]],
      address: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_OTROS)]],
      location: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern(RegularExpressions.ONLY_NUMBERS)]],
      company: ['', [Validators.required]],
      exit: ['', [Validators.required]],
      recommendations: ['', [Validators.required, Validators.pattern(RegularExpressions.LETTERS_SPACES_SPECIALS)]]
    });

    this.getPoint();
    this.getCompanies();
  }

  getPoint() {
    this.transportationService.getPoint(this.param).subscribe(
      res => {
        this.pointGet = res.data

        if(this.pointGet !== null) {
          this.loadData();
        }
      }
    );
  }

  getCompanies() {
    this.transportationService.getCompanies().subscribe(
      res => {
        this.companies = res.data;
      }
    );
  }

  loadData() {
    this.point.get('name').setValue(this.pointGet.nombrePunto);
    this.point.get('address').setValue(this.pointGet.direccion);
    this.point.get('location').setValue(this.pointGet.ubicacionMapa);
    this.point.get('phone').setValue(this.pointGet.telefono);
    this.point.get('company').setValue(this.pointGet.nitEmpresa);
    this.point.get('exit').setValue(this.pointGet.horarioSalida);
    this.point.get('recommendations').setValue(this.pointGet.recomendaciones);
  }

  onSubmit() {
    let data = {
      direccion: this.point.get('address').value,
      horarioSalida: this.point.get('exit').value,
      nitEmpresa: this.point.get('company').value,
      nombrePunto: this.point.get('name').value,
      partitionKey: this.pointGet.partitionKey,
      recomendaciones: this.point.get('recommendations').value,
      rowKey: this.pointGet.rowKey,
      telefono: this.point.get('phone').value,
      ubicacionMapa: this.point.get('location').value
    }

    this.transportationService.updatePoint(data).subscribe(
      response => {
        if (response.resultado[0].codigo == 200) {
          this.showMessage = 'success'
          this.message = response.resultado[0].descripcion

          setTimeout(() => {
            this.return();
          }, 2000);


        } else {
          this.showMessage = 'error'
          this.message = response.resultado[0].descripcion
        }
      }
    );
  }

  public return() {
    this.router.navigate(['/administrador-transporte/puntos-transporte'], {
      relativeTo: this.route
    });
  }

  closeError() {
    this.message = '';
  }

}

