import { UserUpdateComponent } from './pages/user/user-update.component';
import { PointUpdateComponent } from './pages/point/point-update.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransportationAdministratorRoutingModule } from './transportation-administrator-routing';
import { PointComponent } from './pages/point/point.component';
import { TransportationPointsComponent } from './pages/transportation-points/transportation-points.component';
import { TransportersComponent } from './pages/transporters/transporters.component';
import { UserComponent } from './pages/user/user.component';
import {UserRoutingModule} from '../user/user-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CompaniesComponent } from './pages/companies/companies.component';
import { CompanyComponent } from './pages/company/company.component';
import { CompanyUpdateComponent } from './pages/company/company-update.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { SettlementComponent } from './pages/settlement/settlement.component';
import { DemandComponent } from './pages/demand/demand.component';

@NgModule({
  declarations: [
    PointComponent,
    TransportationPointsComponent,
    TransportersComponent,
    UserComponent,
    CompaniesComponent,
    CompanyComponent,
    CompanyUpdateComponent,
    PointUpdateComponent,
    UserUpdateComponent,
    TransactionsComponent,
    SettlementComponent,
    DemandComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TransportationAdministratorRoutingModule
  ]
})
export class TransportationAdministratorModule { }
