import { UserUpdateComponent } from './pages/user/user-update.component';
import { PointUpdateComponent } from './pages/point/point-update.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TransportersComponent} from './pages/transporters/transporters.component';
import {UserComponent} from './pages/user/user.component';
import {RoleGuard} from '../../core/guards/role.guard';
import { TransportationPointsComponent } from './pages/transportation-points/transportation-points.component';
import { CompaniesComponent } from './pages/companies/companies.component';
import { PointComponent } from './pages/point/point.component';
import { CompanyComponent } from './pages/company/company.component';
import { CompanyUpdateComponent } from './pages/company/company-update.component';
import { ERole } from 'src/app/core/models/erole';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { SettlementComponent } from './pages/settlement/settlement.component';
import { DemandComponent } from './pages/demand/demand.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'transacciones',
        component: TransactionsComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'liquidacion',
        component: SettlementComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'demanda',
        component: DemandComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'transportadores',
        component: TransportersComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }
      },
      {
        path: 'puntos-transporte',
        component: TransportationPointsComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      },
      {
        path: 'empresas',
        component: CompaniesComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      },
      {
        path: 'transportador',
        component: UserComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }
      },
      {
        path: 'punto-transporte',
        component: PointComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      },
      {
        path: 'empresa',
        component: CompanyComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      },
      {
        path: 'transportador/:id',
        component: UserUpdateComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }
      },
      {
        path: 'punto-transporte/:id',
        component: PointUpdateComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      },
      {
        path: 'empresa/:id',
        component: CompanyUpdateComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.administrator],
        }
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransportationAdministratorRoutingModule { }
