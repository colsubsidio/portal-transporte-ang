import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UserRoutingModule } from './user-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { RecaptchaModule, RecaptchaFormsModule, RECAPTCHA_SETTINGS, RecaptchaSettings, RECAPTCHA_LANGUAGE} from 'ng-recaptcha';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    RecaptchaModule,
    RecaptchaFormsModule
  ], providers: [
    {
      provide: RECAPTCHA_SETTINGS,
      useValue: { siteKey: '6LdrdXIUAAAAAC1P0GWMYn32rHSdti0Z4nkp4Zu7', } as RecaptchaSettings,
    },
    {
        provide: RECAPTCHA_LANGUAGE,
        useValue: 'es', // use French language
    }
  ]
})
export class UserModule { }
