import { environment } from 'src/app/configs/environment';
import jwt_decode from 'jwt-decode';
import { User } from '../../../../core/models/user';
import { LocalStorageService } from '../../../../core/services/local-storage/local-storage.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OauthService } from 'src/app/core/services/oauth/oauth.service';
import { Router } from '@angular/router';
import { ERole } from 'src/app/core/models/erole';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  loading = false;
  errorServer = false;
  messageAuth = '';
  _USER: User;

  constructor(
    private fb: FormBuilder,
    private authService: OauthService,
    private router: Router,
    private storage: LocalStorageService
  ) { }

  ngOnInit() {
    localStorage.clear();
    this.form = this.fb.group({
      userName: [ '', Validators.required ],
      password: [ '', Validators.required ]
    });
  }

  onSubmit() {
    this.messageAuth = '';
    this.errorServer = false;
    this.loading = true;
    if (this.form.valid) {
      const result = this.form.value;

      this.authService.authUser(result.userName, result.password).subscribe({
        next: (resultData: any) => {
          this.loading = false;
          this.errorServer = false;
          if ( resultData.code === '200' ){
            this.storage.setItem('nav', '');
            const tokenInfo: any = jwt_decode(this.storage.getItemString(environment.loginUserToken));
            if ( tokenInfo !== '' ) {
              tokenInfo.authorities.forEach( ( val: any ) => {
                if ( val === ERole.administrator ) {
                  this.router.navigate(['/administrador-transporte/puntos-transporte']);
                } else if ( val === ERole.company ) {
                  this.router.navigate(['/administrador-transporte/transportadores']);
                } else if ( val === ERole.transporter ) {
                  this.router.navigate(['transportador/inicio']);
                }
              });
            }
          }else{
            this.messageAuth = 'No encontramos tu usuario y contraseña, verifica la información ingresada';
          }
        },
        error: (err: any) => {
          this.messageAuth = 'No encontramos tu usuario y contraseña, verifica la información ingresada';
          this.errorServer = true;
          this.loading = false;
        }
      });
    }
  }

  closeError() {
    this.messageAuth = '';
  }
}
