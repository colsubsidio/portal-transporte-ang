import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DropdownLists } from 'src/app/core/helpers/dropdown-lists/dropdown-lists';
import { LocalStorageService } from 'src/app/core/services/local-storage/local-storage.service';
import { ReservationService } from 'src/app/core/services/qr-transporter/reservation.service';
import { environment } from 'src/app/configs/environment';
import jwt_decode from 'jwt-decode';
import { ModalService } from 'src/app/core/services/modal/modal.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public dataForm: FormGroup;
  public optionsRadio = DropdownLists.transportQR;
  public checkSelect: string;
  tokenInfo: any;
  public name: string;
  public buttonDisabled = true;
  public idModalSpinner = 'loader';

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _storage: LocalStorageService,
    private _reservation_service:ReservationService,
    private _MODAL: ModalService,
    private _changeDetector: ChangeDetectorRef,

  ) {
    this.dataForm = this._formBuilder.group({
      radio: ['', [Validators.required]],
      code: [''],
      numberId: ['']
    });
  }



  ngOnInit(): void {
    this._storage.removeItem(environment.dataConsultPlan);

    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));

    this.name = this.tokenInfo.name;
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  addTitularFormGroup() {
  }

  check(option: string) {
    if (option === 'code') {
      this.dataForm.controls.numberId
      .setValidators(null);
      this.dataForm.controls.numberId.setErrors(null);
      this.dataForm.controls.code
          .setValidators([Validators.required]);
      this.dataForm.controls.code.setValue('');
    } else if (option === 'numberid') {
      this.dataForm.controls.code
          .setValidators(null);
      this.dataForm.controls.code.setErrors(null);
      this.dataForm.controls.numberId
      .setValidators([Validators.required]);
      this.dataForm.controls.numberId.setValue('');
    } else if (option === 'qr') {
      this._router.navigate(['/transportador/qr-scaner']);
    }

    this.checkSelect = option;
  }

  continue() {
    this.buttonDisabled = false;
    this.openModal(this.idModalSpinner);
    const numDoc =  this.dataForm.value.numberId;
    const codeRes = this.dataForm.value.code;
    const code = codeRes === '' ? numDoc : codeRes;
    const type = codeRes === '' ? '2' : '1';
    this.getReservation( type, code);

  }

  getReservation( type: string, code: string ) {
    this._reservation_service.getInfoReservation(type, code).subscribe(
      res => {
        this.closeModal(this.idModalSpinner);

        if ( res != null && res.resultado[0].codigo === 200 ){
          this._storage.stringifyItem(environment.dataConsultPlan, res.data);
          this._router.navigate(['/transportador/informacion-plan']);
        }else{
          this._storage.setItem('show', 'errorHome');
          this._router.navigate(['/transportador/proceso-error']);
          this.buttonDisabled = true;
        }
      }
    );
  }

  closeModal(id) {
    this._MODAL.closeModal(id);
  }

  openModal(id) {
    this._changeDetector.detectChanges();
    this._MODAL.openModal(id);
  }

}
