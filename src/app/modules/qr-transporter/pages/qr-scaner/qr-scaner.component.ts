import { ModalService } from 'src/app/core/services/modal/modal.service';
import { ReservationService } from 'src/app/core/services/qr-transporter/reservation.service';
import { LocalStorageService } from 'src/app/core/services/local-storage/local-storage.service';
import { Router } from '@angular/router';
import { Component, ChangeDetectorRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { QrScannerComponent } from 'angular2-qrscanner';
import { environment } from 'src/app/configs/environment';

@Component({
  selector: 'app-qr-scaner',
  templateUrl: './qr-scaner.component.html',
  styleUrls: ['./qr-scaner.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QrScanerComponent {

  @ViewChild( QrScannerComponent ) qrScannerComponent: QrScannerComponent;

  idModalSpinner = 'loader';

  constructor(
    private _ROUTER: Router,
    private _STORAGE: LocalStorageService,
    private _RESERVATION: ReservationService,
    private _MODAL: ModalService,
    private _CHANGE: ChangeDetectorRef,
  ) { }

  ngAfterViewInit(): void {
    this._CHANGE.detectChanges();
    this.qrScannerComponent.getMediaDevices().then(devices => {
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
          if (device.kind.toString() === 'videoinput') {
              videoDevices.push(device);
          }
      }
      if (videoDevices.length > 0){
          let choosenDev;
          for (const dev of videoDevices){
              if (dev.label.includes('front')){
                  choosenDev = dev;
                  break;
              }
          }
          if (choosenDev) {
              this.qrScannerComponent.chooseCamera.next(choosenDev);
          } else {
              this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
          }
      }
    });

    this.qrScannerComponent.capturedQr.subscribe( ( code: string ) => {
      this._CHANGE.detectChanges();
      this.getReservation( '1', code );
    });

  }

  getReservation( type: string, code: string ) {
    this.openModal(this.idModalSpinner);
    this._RESERVATION.getInfoReservation(type, code).subscribe(
      res => {
        this.closeModal(this.idModalSpinner);
        if ( res != null && res.resultado[0].codigo === 200 ){
          this._STORAGE.stringifyItem(environment.dataConsultPlan, res.data);
          this._ROUTER.navigate(['/transportador/informacion-plan']);
        }else{
          this._STORAGE.setItem('show', 'errorHome');
          this._ROUTER.navigate(['/transportador/proceso-error']);
        }
      }
    );
  }

  backView() {
    this._ROUTER.navigate(['/transportador/inicio']);
  }

  closeModal(id) {
    this._MODAL.closeModal(id);
  }

  openModal(id) {
    this._CHANGE.detectChanges();
    this._MODAL.openModal(id);
  }

}
