import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrScanerComponent } from './qr-scaner.component';

describe('QrScanerComponent', () => {
  let component: QrScanerComponent;
  let fixture: ComponentFixture<QrScanerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrScanerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrScanerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
