import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from 'src/app/core/services/local-storage/local-storage.service';
import { IinfoReservation } from 'src/app/core/models/iinfo-reservation';
import { environment } from 'src/app/configs/environment';
import { RedemptionPetition } from 'src/app/core/models/redemption-petition';
import { EconsumptionType } from 'src/app/core/models/econsumption-type.enum';
import { ReservationService } from 'src/app/core/services/qr-transporter/reservation.service';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-plan-information',
  templateUrl: './plan-information.component.html',
  styleUrls: ['./plan-information.component.scss']
})
export class PlanInformationComponent implements OnInit {
  public tokenInfo: any;
  public dataForm: FormGroup;
  public infoReservation:IinfoReservation;
  public namePerson: string;
  public buttonDisabled: boolean;
  public going: boolean;
  public back: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private _storage: LocalStorageService,
    private _reservation_service:ReservationService,
  ) {
    this.dataForm = this._formBuilder.group({
      journey: ['']
    });
  }

  ngOnInit(): void {
    this.fillDataReservation();
    this.tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));

    this.buttonDisabled = false;
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  fillDataReservation(){
    if(this._storage.existItem(environment.dataConsultPlan)){
      this.infoReservation = this._storage.parseItem<IinfoReservation>(environment.dataConsultPlan);
      this.checkRoutes(this.infoReservation);
      this.namePerson = this.infoReservation.cliente.primerNombre
                        +this.getTextEmpty(this.infoReservation.cliente.segundoNombre)
                        +this.getTextEmpty(this.infoReservation.cliente.primerApellido)
                        +this.getTextEmpty(this.infoReservation.cliente.segundoApellido);
    }else{
      this.router.navigate(['/transportador/inicio']);
    }
  }

  checkRoutes(infoReservation:IinfoReservation){
    if(!infoReservation.disponibilidadTrayecto.ida){
    /*this.selectAndDisableControl('going');
      this.dataForm.get('back').setValidators([Validators.required])
      this.dataForm.get('going').updateValueAndValidity();*/
      var b = document.querySelector("#going");
      b.setAttribute("disabled", "");
    }

    if(!infoReservation.disponibilidadTrayecto.regreso){
      /*this.selectAndDisableControl('back');
      this.dataForm.get('going').setValidators([Validators.required])
      this.dataForm.get('back').updateValueAndValidity();*/
      var b = document.querySelector("#back");
      b.setAttribute("disabled", "");
    }
  }

  validateCheck() {
    let back = this.dataForm.value.back;
    let going = this.dataForm.value.going;

    if (back !== false && back !== '') {
      this.buttonDisabled = true;
    } else if (going !== false && going !== '') {
      this.buttonDisabled = true;
    } else {
      this.buttonDisabled = false;
    }
  }

  selectAndDisableControl(control: string) {
    console.log(control);
    this.dataForm.get(control).setValue('1');
      this.dataForm.get(control).disable()
  }

  getTextEmpty(value: string){
    return (value !== '')?' '+value:value;
  }

  continue() {
    console.log(this.dataForm.get('journey').value)
    const redemptionPetition = new RedemptionPetition();
    redemptionPetition.numeroDocumentoCliente = this.infoReservation.cliente.numDoc;
    redemptionPetition.numeroDocumentoConductor = this.tokenInfo.numDoc;
    //redemptionPetition.tipoConsumo = this.getSelectCheckJourney();
    redemptionPetition.tipoConsumo = this.dataForm.get('journey').value;

    if(redemptionPetition.tipoConsumo !== ''){
      this._reservation_service.redeemPackage(redemptionPetition).subscribe(
        res=>{
          if(res.resultado[0].codigo == '200'){
            this.router.navigate(['/transportador/proceso-exitoso']);
          }else{
            this._storage.setItem('show', 'errorPlan');
            this.router.navigate(['/transportador/proceso-error']);
          }
        }
      );
    }
  }

  getSelectCheckJourney():string{
    let journey = '';
    if(this.infoReservation.disponibilidadTrayecto.ida){
        journey = EconsumptionType.Going;
    }

    if(this.infoReservation.disponibilidadTrayecto.regreso){
        journey = EconsumptionType.Back;
    }
    return journey;
  }

  return() {
    this.router.navigate(['/transportador/inicio'], {
      relativeTo: this.route
    });
  }
}
