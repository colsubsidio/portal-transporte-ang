import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/core/services/local-storage/local-storage.service';

@Component({
  selector: 'app-message-error',
  templateUrl: './message-error.component.html',
  styleUrls: ['./message-error.component.scss']
})
export class MessageErrorComponent implements OnInit {

  titleHome = 'Información inválida';
  bodyHome = `Confirma que el código de reserva o número de documento hayan sido ingresados correctamente.
              Si este error persiste, no existe un servicio de transporte asociado a esta información o
              que sea válido a la fecha.`;

  titlePlan = 'Entrada o información inválida';
  bodyPlan = `Por favor confirma que la información ingresada sea correcta o que las entradas hayan sido
              escaneadas correctamente. Si este error persiste, el usuario no tiene entradas disponibles
              o válidas a la fecha.`;

  textButton = 'Volver al inicio';
  link = '/transportador/inicio';

  show: string;

  constructor(
    private _storage: LocalStorageService,
  ) {
    this.show = this._storage.getItemString('show')
  }

  ngOnInit(): void {
  }

}
