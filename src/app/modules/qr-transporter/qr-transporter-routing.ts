import { QrScanerComponent } from './pages/qr-scaner/qr-scaner.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import {RoleGuard} from '../../core/guards/role.guard';
import { ERole } from 'src/app/core/models/erole';
import { PlanInformationComponent } from './pages/plan-information/plan-information.component';
import { MessageErrorComponent } from './pages/message-error/message-error.component';
import { MessageSuccessfulComponent } from './pages/message-successful/message-successful.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'inicio',
        component: HomeComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.transporter],
        }
      },
      {
        path: 'qr-scaner',
        component: QrScanerComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.transporter],
        }
      },
      {
        path: 'informacion-plan',
        component: PlanInformationComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.transporter],
        }
      },
      {
        path: 'proceso-exitoso',
        component: MessageSuccessfulComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.transporter],
        }
      },
      {
        path: 'proceso-error',
        component: MessageErrorComponent,
        canActivate: [RoleGuard],
        data: {
          roles: [ERole.transporter],
        }
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrTransporterRoutingModule { }
