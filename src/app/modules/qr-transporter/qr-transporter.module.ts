import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from '../user/user-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { PlanInformationComponent } from './pages/plan-information/plan-information.component';
import { QrTransporterRoutingModule } from './qr-transporter-routing';
import { HomeComponent } from './pages/home/home.component';
import { MessageErrorComponent } from './pages/message-error/message-error.component';
import { MessageSuccessfulComponent } from './pages/message-successful/message-successful.component';
import { NgQrScannerModule } from 'angular2-qrscanner';
import { QrScanerComponent } from './pages/qr-scaner/qr-scaner.component';

@NgModule({
  declarations: [
    HomeComponent,
    PlanInformationComponent,
    MessageErrorComponent,
    MessageSuccessfulComponent,
    QrScanerComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    QrTransporterRoutingModule,
    NgQrScannerModule
  ]
})
export class QrTransporterModule{ }
