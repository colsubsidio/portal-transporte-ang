import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { NewsletterComponent } from './pages/newsletter/newsletter.component';
import { EventsComponent } from './pages/events/events.component';
import { SettingComponent } from './pages/setting/setting.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'transacciones',
        component: TransactionsComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
       {
        path: 'eventos',
        component: EventsComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
       {
        path: 'newsletter',
        component: TransactionsComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      },
       {
        path: 'configuracion',
        component: SettingComponent,
        /*canActivate: [RoleGuard],
        data: {
          roles: [ERole.company],
        }*/
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ColsubsidioAdministratorRoutingModule { }
