import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

  showDes: boolean = false;
  public dates: FormGroup;
  public purchase: FormGroup;
  public infants: FormGroup;
  public addUser: FormGroup;

  public closeFil: boolean;

  public maxCon = '2021-04-15T10:25:00.801Z';
  public minCon = new Date();

  public maxCon2 = '2021-07-15T10:25:00.801Z';
  public minCon2 = '2021-04-15T10:25:00.801Z';

  transactions = [
    {
      nombre: '123456'
    },
    {
      nombre: '09876'
    }
  ];

  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.dates = this._formBuilder.group({
      since: ['', [Validators.required]],
      until: ['', [Validators.required]]
    });

    this.purchase = this._formBuilder.group({
      purchase: ['', [Validators.required]]
    });

    this.infants = this._formBuilder.group({
      infantsAge: ['', [Validators.required]]
    });

    this.addUser = this._formBuilder.group({
      names: ['', [Validators.required]],
      documentNumber: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.closeFil = false;
  }

  continue() {
  }

}
