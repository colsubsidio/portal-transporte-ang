import { TransportationService } from '../../../../core/services/trabsportation/transportation.service';
import { map } from 'rxjs/operators';
import { HttpService } from '../../../../core/services/http/http.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/app/configs/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { Table } from 'src/app/configs/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {

  showDes: boolean = false;
  public dataForm: FormGroup;
  public closeFil: boolean;

  public maxCon = '2021-03-15T10:25:00.801Z';
  public minCon = new Date();

  public maxCon2 = new Date();
  public minCon2 = '2021-01-15T10:25:00.801Z';

  transactions = [
    {
      nombre: '123456'
    },
    {
      nombre: '09876'
    }
  ];
  headTable = Table.tableTransactions;

  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.dataForm = this._formBuilder.group({
      transporter: [''],
      since: [''],
      until: ['']
    });
  }

  ngOnInit(): void {
    this.closeFil = false;
  }

  closeFilter(value) {
    if (value == 'abrir') {
      this.closeFil = true;
    } else {
      this.closeFil = false;
    }
  }

  continue() {
    this.closeFilter('cerrar')
  }

}
