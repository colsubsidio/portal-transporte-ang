import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { NewsletterComponent } from './pages/newsletter/newsletter.component';
import { EventsComponent } from './pages/events/events.component';
import { SettingComponent } from './pages/setting/setting.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { ColsubsidioAdministratorRoutingModule } from './colsubsidio-administrator-routing';


@NgModule({
  declarations: [
    TransactionsComponent,
    NewsletterComponent,
    EventsComponent,
    SettingComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ColsubsidioAdministratorRoutingModule
  ]
})
export class ColsubsidioAdministratorModule { }
