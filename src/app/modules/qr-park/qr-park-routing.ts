import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import {RoleGuard} from '../../core/guards/role.guard';
import { ERole } from 'src/app/core/models/erole';
import { PlanInformationComponent } from './pages/plan-information/plan-information.component';
import { MessageSuccessfulComponent } from './pages/message-successful/message-successful.component';
import { MessageErrorComponent } from './pages/message-error/message-error.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'inicio',
        component: HomeComponent,
        //canActivate: [RoleGuard],
        /*data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'informacion-plan',
        component: PlanInformationComponent,
        //canActivate: [RoleGuard],
        /*data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'proceso-exitoso',
        component: MessageSuccessfulComponent,
        //canActivate: [RoleGuard],
        /*data: {
          roles: [ERole.company],
        }*/
      },
      {
        path: 'proceso-error',
        component: MessageErrorComponent,
        //canActivate: [RoleGuard],
        /*data: {
          roles: [ERole.company],
        }*/
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrParkRoutingModule { }
