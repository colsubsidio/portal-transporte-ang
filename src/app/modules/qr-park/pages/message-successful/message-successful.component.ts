import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-successful',
  templateUrl: './message-successful.component.html',
  styleUrls: ['./message-successful.component.scss']
})
export class MessageSuccessfulComponent implements OnInit {

  info = '¡Has registrado la utilización del servicio con éxito!';
  textButton = 'Volver al inicio';
  link = '/transportador/inicio';

  constructor() { }

  ngOnInit(): void {
  }

}
