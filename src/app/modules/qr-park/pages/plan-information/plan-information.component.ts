import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-plan-information',
  templateUrl: './plan-information.component.html',
  styleUrls: ['./plan-information.component.scss']
})
export class PlanInformationComponent implements OnInit {
  public dataForm: FormGroup;

  constructor(
    private _formBuilder: FormBuilder,
  ) {
    this.dataForm = this._formBuilder.group({
      going: [''],
      entry: [''],
      lunch: [''],
      refreshment: [''],
      back: ['']
    });
  }

  ngOnInit(): void {
  }

  continue() {

  }

}
