import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { RegularExpressions } from 'src/app/core/helpers/regular-expressions/regular-expressions';
import { DropdownLists } from 'src/app/core/helpers/dropdown-lists/dropdown-lists';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public dataForm: FormGroup;
  public optionsRadio = DropdownLists.transportQR;

  constructor(
    private _formBuilder: FormBuilder,
    private _changeDetector: ChangeDetectorRef,
    private _router: Router,
    private _route: ActivatedRoute,
  ) {
    this.dataForm = this._formBuilder.group({
      radio: ['', [Validators.required]],
      code: ['']
    });
  }

  ngOnInit(): void {
  }

  addTitularFormGroup() {
  }

  continue() {}

}
