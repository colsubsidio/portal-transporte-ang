import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE, } from 'ng-pick-datetime';
import { from } from 'rxjs';
import { CalendarModule } from 'angular-calendar';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FormErrorsComponent } from './components/form-errors/form-errors.component';
import { InputTextComponent } from './components/input-text/input-text.component';
import { SelectComponent } from './components/select/select.component';
import { AccordionComponent } from './components/accordion/accordion.component';
import { PanelComponent } from './components/accordion/panel/panel.component';
import { TableComponent } from './components/table/table.component';
import { InputRadioCheckComponent } from './components/input-radio-check/input-radio-check.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { ErrorComponent } from './components/error/error.component';
import { SuccessfulComponent } from './components/successful/successful.component';
import { ModalSpinnerComponent } from './components/modal-spinner/modal-spinner.component';
import { RadioComponent } from './components/radio/radio.component';
export const MY_NATIVE_FORMATS = {
  fullPickerInput: { day: 'numeric', month: 'numeric', year: 'numeric' },
  datePickerInput: { day: 'numeric', month: 'numeric', year: 'numeric' },
  timePickerInput: { hour: 'numeric', minute: 'numeric' },
  monthYearLabel: { year: 'numeric', month: 'short' },
  dateA11yLabel: { year: 'numeric', month: 'numeric', day: 'numeric' },
  monthYearA11yLabel: { year: 'numeric', month: 'numeric' }
};

@NgModule({
  declarations: [
    FormErrorsComponent,
    InputTextComponent,
    SelectComponent,
    AccordionComponent,
    PanelComponent,
    TableComponent,
    InputRadioCheckComponent,
    CheckboxComponent,
    ErrorComponent,
    SuccessfulComponent,
    ModalSpinnerComponent,
    RadioComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CalendarModule,
    AutocompleteLibModule,
    RouterModule
  ],
  exports: [
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    CalendarModule,
    AutocompleteLibModule,
    FormsModule,
    ReactiveFormsModule,
    FormErrorsComponent,
    InputTextComponent,
    SelectComponent,
    AccordionComponent,
    PanelComponent,
    TableComponent,
    InputRadioCheckComponent,
    CheckboxComponent,
    ErrorComponent,
    SuccessfulComponent,
    ModalSpinnerComponent,
    RadioComponent
  ],
  providers: [
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'es' },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS },
  ]
})
export class SharedModule { }
