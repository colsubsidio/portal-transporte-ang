import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})
export class RadioComponent implements OnInit {
  @Input() public disabled: boolean;
  @Input() public nameItem: string;
  @Input() public valueItem: string;
  @Input() public textLabel: string;
  @Input() public idItem: string;
  @Input() public type: string;
  @Input() public frmControl: FormControl;
  @Input() public text: string;
  @Input() public image?: string;
  @Input() public alt?: string;

  constructor() { }

  ngOnInit(): void {
  }

  
  change() {

  }

}
