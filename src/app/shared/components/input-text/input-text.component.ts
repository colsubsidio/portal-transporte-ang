import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit {
  @Input() public disabled: boolean;
  @Input() public nameItem: string;
  @Input() public valueItem: string;
  @Input() public textLabel: string;
  @Input() public idItem: string;
  @Input() public type: string;
  @Input() public frmControl: FormControl;
  @Input() public image?: string;
  @Input() public alt?: string;
  @Input() public placeHolder?: string;

  constructor() { }

  ngOnInit(): void {
  }

  showPassword() {
    let type = document.getElementById(this.idItem) as any;
      if(type.type == "password"){
        type.type = "text";
      }else{
        type.type = "password";
      }
  }

}
