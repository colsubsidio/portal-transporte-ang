import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
  @Input() public disabled: boolean;
  @Input() public nameSelect: string;
  @Input() public valueSelect: string;
  @Input() public textSelect: string;
  @Input() public idSelect: string;
  @Input() public frmControl: FormControl;

  constructor() { }

  ngOnInit(): void {
  }

}
