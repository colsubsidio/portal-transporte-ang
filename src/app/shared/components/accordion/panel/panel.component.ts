import { LocalStorageService } from './../../../../core/services/local-storage/local-storage.service';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  @Input('title') title;
  @Input('path') path;
  @Output() onToggle = new EventEmitter();

  show = false;

  constructor(private router: Router, private storage: LocalStorageService) { }

  ngOnInit() {
  }

  toggle() {
    this.storage.setItem('item', this.title)
    this.show = !this.show;
    this.onToggle.next({
      show: this.show,
    })

    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>
    this.router.navigate([this.path]));

    if(this.storage.getItemString('list') === this.title) {
      this.show = false
    }
  }

}
