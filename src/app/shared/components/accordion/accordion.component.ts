import { LocalStorageService } from './../../../core/services/local-storage/local-storage.service';
import { Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { PanelComponent } from './panel/panel.component';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {

  @ContentChildren(PanelComponent) panels: QueryList<PanelComponent>;

  constructor(private storage: LocalStorageService) { }

  ngOnInit() {
  }

  onToggleSubscriber(p) {
    return data => {
      this.panels.forEach(panel => {
        if (panel['title'] === p) {
          panel.show = true;
        }
      })
    }
  }

  ngAfterContentInit() {
    this.panels.forEach((panel, i) => {
      if (panel['title'] === this.storage.getItemString('item')) {
        this.storage.setItem('list', panel.title);
        panel.onToggle.subscribe(this.onToggleSubscriber(panel))
        panel.show = i < 1;
      }
    })
  }

}
