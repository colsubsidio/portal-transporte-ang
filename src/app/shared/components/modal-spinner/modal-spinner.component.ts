import { Component, OnInit, Input } from '@angular/core';
import { ModalService } from 'src/app/core/services/modal/modal.service';

@Component({
  selector: 'app-modal-spinner',
  templateUrl: './modal-spinner.component.html',
  styleUrls: ['./modal-spinner.component.scss']
})
export class ModalSpinnerComponent implements OnInit {
  @Input() idModal: string;

  constructor(
    private _MODAL: ModalService
  ) { }

  ngOnInit(): void {
  }

  close() {
    this._MODAL.closeModal(this.idModal);
  }

}
