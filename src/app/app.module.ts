import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import localeEsCo from '@angular/common/locales/es-CO';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { FooterComponent } from './layouts/main-layout/utils/footer/footer.component';
import { HeaderComponent } from './layouts/main-layout/utils/header/header.component';
import { SharedModule } from './shared/shared.module';
import { NavDashComponent } from './layouts/main-layout/utils/nav-dash/nav-dash.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BreadcrumbComponent } from './layouts/main-layout/utils/breadcrumb/breadcrumb.component';
import { AuthTokenProvider } from './core/interceptors/auth/auth-interceptor.service';
import { registerLocaleData } from '@angular/common';
import { QrLayoutComponent } from './layouts/qr-layout/qr-layout.component';
import { HeaderQrComponent } from './layouts/qr-layout/utils/header-qr/header-qr.component';
import { FooterQrComponent } from './layouts/qr-layout/utils/footer-qr/footer-qr.component';
import { NgQrScannerModule } from 'angular2-qrscanner';

registerLocaleData(localeEsCo, 'es-CO');

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    FooterComponent,
    HeaderComponent,
    NavDashComponent,
    BreadcrumbComponent,
    QrLayoutComponent,
    HeaderQrComponent,
    FooterQrComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    BrowserAnimationsModule,
    NgQrScannerModule
  ],
  providers: [
    AuthTokenProvider,
    {provide: LOCALE_ID, useValue: 'es-CO'},],
  bootstrap: [AppComponent]
})
export class AppModule { }
