export interface Resultado {
  codigo: number;
  descripcion: string;
}

export interface Demand {
  fechaTransporte: string;
  puntoRecogida: string;
  totalPersonas: number;
}

export interface ResponseDemand {
  resultado: Resultado[];
  data: Demand[];
}
