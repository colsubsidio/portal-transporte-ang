export class IgeneralResponseTwo<T> {
    resultado:Array<Resultado>;
    data:T;
}

export interface Resultado {
    codigo: any;
    descripcion: string;
}
