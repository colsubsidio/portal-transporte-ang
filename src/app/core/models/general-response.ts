export interface GeneralResponse<T> {
  code: string;
  body: T;
  success: boolean;
  message: string;
}
