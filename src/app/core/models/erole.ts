export enum ERole {
  administrator = 'ROLE_ADMIN',
  transporter = 'ROLE_TRANSPORT',
  company = 'ROLE_COMPANY'
}
