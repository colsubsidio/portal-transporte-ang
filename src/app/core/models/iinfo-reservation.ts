
export interface IinfoReservation {
    reserva: Reserva;
    cliente: Cliente;
    disponibilidadTrayecto: DisponibilidadTrayecto;
    
}

export interface Reserva {
    codigo: string;
    detalle: string;
    fecha: string;
}

export interface Cliente {
    primerApellido: string;
    primerNombre: string;
    segundoApellido: string;
    segundoNombre: string;
    numDoc: string;
}

export interface DisponibilidadTrayecto {
    ida: boolean;
    regreso: boolean;
}
