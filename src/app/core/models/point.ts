export interface Point {
  partitionKey: string
  rowKey: string
  etag: string
  nombrePunto: string
  direccion: string
  ubicacionMapa: string
  telefono: string
  horarioSalida: string
  recomendaciones: string
  nitEmpresa: string
  fechaCreado: string
  fechaActualizado: any
  fechaEliminado: string
  timestamp: string
}
