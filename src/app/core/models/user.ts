export class User {
  username: string;
  token: string;
  exp: number;
  iat: number;
  jti: string;
  sub: string;
  name: string;
  roles: Array<string> = [];
}
