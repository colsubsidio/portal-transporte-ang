import { Component } from '@angular/core';
export interface MenuItems {
  name: string;
  icon: string;
  path: string;
  nameEvent: string;
  roles: Array<string>;
  childrens: Array<MenuItemsChildren>;
  component: string;
}

export interface MenuItemsChildren{
  name: string;
  icon: string;
  path: string;
  roles: Array<string>;
}
