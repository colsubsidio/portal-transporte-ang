export interface Company {
  partitionKey: string
  rowKey: string
  etag: string
  usuario: string
  numeroDocumento: string
  tipoDocumento: string
  clave: string
  correo: string
  nombreCompleto: string
  estado: string
  perfil: string
  rowKeyEmpresa: any
  fechaCreado: string
  fechaActualizado: string
  fechaEliminado: string
  timestamp: string
}
