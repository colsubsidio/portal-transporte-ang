
export interface Resultado {
    codigo: number;
    descripcion: string;
}

export interface Transportador {
    nombreCompleto: string;
    numDoc: string;
}

export interface Transportadora {
    nombreCompleto: string;
    nit: string;
}

export interface Transporte {
    idReserva: string;
    valor: string;
    documentoConductor: string;
    fechaReserva: string;
    fechaCreado: Date;
    fechaIda: string;
    fechaRegreso: string;
    fechaEliminado: string;
    transportador: Transportador;
    transportadora: Transportadora;
    puntoRecogida: string;
}

export interface Reserva {
    codigo?: any;
    detalle?: any;
    fecha?: any;
    transporte: Transporte;
}

export interface Cliente {
    primerApellido: string;
    primerNombre: string;
    segundoApellido: string;
    segundoNombre: string;
    numDoc: string;
}

export interface Settlement {
    reserva: Reserva;
    cliente: Cliente;
}

export interface ResponseSettlement {
    resultado: Resultado[];
    data: Settlement[];
}

export class SettlementCsv {
  fechaTransporte: string;
  codigoReserva: string;
  tipoDoc: string;
  numeroDoc: string;
  nombreCompletoCliente: string;
  empresaTransportadora: string;
  puntoRecogida: string;
  nombreConductor: string;
  fechaHoraIda: string;
  fechaHoraRegreso: string;
  numeroDocConductor: string;
}

export class TransactionsCsv {
  fechaTransporte: string;
  codigoReserva: string;
  tipoDoc: string;
  numeroDoc: string;
  nombreCompletoCliente: string;
  empresaTransportadora: string;
  puntoRecogida: string;
  nombreConductor: string;
  fechaHoraIda: string;
  fechaHoraRegreso: string;
  numeroDocConductor: string;
}

