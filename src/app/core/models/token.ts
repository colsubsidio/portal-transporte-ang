export interface Token {
  jti: string;
  sub: string;
  name: string;
  numDoc: string;
  authorities: string[];
  iat: number;
  exp: number;
}
