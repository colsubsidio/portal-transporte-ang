import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/app/configs/environment';
import { GeneralResponse } from '../../models/general-response';
import { ICentroLogistico } from '../../models/icentro-logistico';
import { IDepartament } from '../../models/idepartament';
import { IDirectiva } from '../../models/idirectiva';
import { ITableList } from '../../models/itable-list';
import { IUnidadNegocio } from '../../models/iunidad-negocio';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ListsService {

  constructor(private _HTTP: HttpService) { }

  getDepartamentos():Observable<Array<ITableList>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/departamentos',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<ITableList>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getMunicipios(idDepto: string):Observable<Array<ITableList>>{
    const headers = { 'Content-Type': 'application/json' };
    const params = {idDepto};
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/municipios',
      params,
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<ITableList>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getNacionalidades():Observable<Array<ITableList>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/nacionalidades',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<ITableList>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getLists():Observable<any>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/listas',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<any>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getOrganizacionDptos():Observable<Array<IDepartament>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/organizacion/departamentos',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<IDepartament>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getOrganizacionDirectivas():Observable<Array<IDirectiva>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/organizacion/directivas',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<IDirectiva>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }

  getCentrosLogisticos():Observable<Array<ICentroLogistico>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._get(
      environment.URL_ADSCRITOS_ADMIN,
      '/organizacion/centroslogis',
      {},
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<Array<ICentroLogistico>>;
        return result.body;
      },err=>{
        return null;
      })
    );
  }
}
