import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/app/configs/environment';
import { GeneralResponse } from '../../models/general-response';
import { User } from '../../models/user';
import { HttpService } from '../http/http.service';
import { LocalStorageService } from '../local-storage/local-storage.service';
import jwt_decode from "jwt-decode";
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class OauthService {

  _USER: User;
  private loggedIn = new BehaviorSubject<boolean>(false);
  constructor(private _HTTP: HttpService,
              private _LOCAL_STORAGE: LocalStorageService,
              private router: Router) { }

  authUser(username: string, password: string): Observable<GeneralResponse<User>>{
    const params = {username, password};
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._post(
      environment.URL_USER_AUTH,
      '/usuario/autenticacion',
      {},
      params,
      headers
    ).pipe(
      map((response) => {
        const result = response as GeneralResponse<User>;
        this._LOCAL_STORAGE.removeItem(environment.loginUserToken);
        if (result.body){
          this._LOCAL_STORAGE.setItem(environment.loginUserToken, result.body.token.replace('Bearer ', ''));
          this.fillInfoUser(localStorage.getItem(environment.loginUserToken));
        }
        return result;
      },err => {
        return null;
      })
    );
  }

  public get user(): User {
    if (localStorage.getItem(environment.loginUserToken) != null) {
      this.fillInfoUser(localStorage.getItem(environment.loginUserToken));
      return this._USER;
    } else {
      return new User();
    }
  }

  fillInfoUser(token: string){
    const tokenInfo = this.getDataToken(token);
    this._USER = new User();
    this._USER.name = tokenInfo.name;
    this._USER.username = tokenInfo.sub;
    this._USER.roles = new Array<string>();
    tokenInfo.authorities.forEach(element => {
      this._USER.roles.push(element);
    });
  }

  getDataToken(token: string): any {
    if (token != null) {
      return jwt_decode(token);
    }
    return null;
  }

  hasRole(role: string): boolean {
    if (this.user.roles.includes(role)) {
      return true;
    }
    return false;

    //return true
  }

  hasArrayRole(roles: Array<string>): boolean {
    let hasRole = false;
    roles.forEach(item => {
      hasRole = (hasRole) ? hasRole : this.hasRole(item);
    });
    return hasRole;
    //return true
  }

  logout() {
    this.loggedIn.next(false);
    this._USER = null;
    localStorage.clear();
    this.router.navigate(['/']);
  }

  validateDataToken():boolean{
    const tokenInfo = this.getDataToken(localStorage.getItem(environment.loginUserToken));
    const fechaAuth = tokenInfo.exp;
    const datePru = new Date(0);
    let dateExpToken = datePru.setUTCSeconds(fechaAuth);
    if(new Date().valueOf() > dateExpToken.valueOf()){
      return false;
    }
    return true;
  }

}
