import { Injectable, Output, EventEmitter } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  toogleProfile = true;
  toogle = true;

  private subject: Subject<boolean> = new Subject<boolean>();
  private subjectProfile: Subject<boolean> = new Subject<boolean>();

  @Output() changeToggle: EventEmitter<boolean> = new EventEmitter();
  isOpen = false;


  constructor() {}

  toggle() {
    this.isOpen = !this.isOpen;
    this.changeToggle.emit(this.isOpen);
  }

  getMenuStatus() {
    return this.subject.asObservable();
  }

  getMenuStatusProfile() {
    return this.subjectProfile.asObservable();
  }

  toogleMenu( toogle: boolean ) {
    this.subject.next(toogle);
    this.toogle = !this.toogle;
  }

  toogleMenuProfile( toogleProfile: boolean ) {
    this.subjectProfile.next(toogleProfile);
    this.toogleProfile = this.toogleProfile;
  }
}
