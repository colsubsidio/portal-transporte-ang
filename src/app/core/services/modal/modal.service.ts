import { Injectable } from '@angular/core';
import MicroModal from 'micromodal';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor() { }

  openModal(idModal: string) {
    MicroModal.show(idModal);
  }

  closeModal(idModal: string) {
    MicroModal.close(idModal);
  }
}
