import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { environment } from 'src/app/configs/environment';
import { map } from 'rxjs/operators';
import { GeneralResponse } from '../../models/general-response';

@Injectable({
  providedIn: 'root'
})
export class TransportationService {

  constructor(private _HTTP: HttpService) { }

  public getTransporters(nit) {
    return this._HTTP.get<any>(`${environment.END_POINT}/usuarios?nit=${nit}`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public getPoints() {
    return this._HTTP.get<any>(`${environment.END_POINT}/puntos`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public getPointsForBusiness(nitBusiness: string) {
    return this._HTTP.get<any>(`${environment.END_POINT}/puntos?nitEmpresa=${nitBusiness}`)
    .pipe(
      map(
        response => response.data
      )
    );
  }

  public getCompanies() {
    return this._HTTP.get<any>(`${environment.END_POINT}/empresas`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public getTransporter(id) {
    return this._HTTP.get<any>(`${environment.END_POINT}/usuarios/${id}`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public getPoint(id) {
    return this._HTTP.get<any>(`${environment.END_POINT}/puntos/${id}`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public getCompany(id) {
    return this._HTTP.get<any>(`${environment.END_POINT}/empresas/${id}`)
    .pipe(
      map(
        response => response
      )
    );
  }

  public createTransporter (data) {
    return this._HTTP.post<any>(`${environment.END_POINT}/usuarios`, data)
    .pipe(
      map(
        response => response
      )
    );
  }

  public createPoint (data) {
    return this._HTTP.post<any>(`${environment.END_POINT}/puntos`, data)
    .pipe(
      map(
        response => response
      )
    );
  }

  public createCompany (data) {
    return this._HTTP.post<any>(`${environment.END_POINT}/empresas`, data)
    .pipe(
      map(
        response => response
      )
    );
  }


  public updateTransporter(data) {
    return this._HTTP.put<any>(`${environment.END_POINT}/usuarios`, data)
    .pipe(
      map(
        response => response
      )
    )
  }

  public updatePoint(data) {
    return this._HTTP.put<any>(`${environment.END_POINT}/puntos`, data)
    .pipe(
      map(
        response => response
      )
    )
  }

  public updateCompany(data) {
    return this._HTTP.put<any>(`${environment.END_POINT}/empresas`, data)
    .pipe(
      map(
        response => response
      )
    )
  }

  public deleteTransporter(id) {
    return this._HTTP.delete<any>(`${environment.END_POINT}/usuarios/${id}`)
    .pipe(
      map(
        response => response
      )
    )
  }

  public deletePoint(id) {
    return this._HTTP.delete<any>(`${environment.END_POINT}/puntos/${id}`)
    .pipe(
      map(
        response => response
      )
    )
  }

  public deleteCompany(id) {
    return this._HTTP.delete<any>(`${environment.END_POINT}/empresas/${id}`)
    .pipe(
      map(
        response => response
      )
    )
  }

}
