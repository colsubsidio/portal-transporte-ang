import { ResponseDemand } from './../../models/demand';
import { ResponseSettlement } from './../../models/settlement';
import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { environment } from 'src/app/configs/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  constructor(private _HTTP: HttpService) { }

  public getSettlement( nitBusiness: string, transportationPoint: string, initDate: string, lastDate: string ) {
    let url = `${environment.END_POINT_REPORT}/liquidacion?nitEmpresa=${nitBusiness}`;
    url = url + `&puntoTransporte=${transportationPoint}&fechaInicial=${initDate}&fechaFinal=${lastDate}`;
    return this._HTTP.get<any>(url)
    .pipe(
      map(
        (response: ResponseSettlement) => response.data
      )
    );
  }

  public getTransaction( nitBusiness: string, initDate: string, lastDate: string ) {
    const url = `${environment.END_POINT_REPORT}/transacciones?nitEmpresa=${nitBusiness}&fechaInicial=${initDate}&fechaFinal=${lastDate}`;
    return this._HTTP.get<any>(url)
    .pipe(
      map(
        (response: ResponseSettlement) => response.data
      )
    );
  }

  public getDemand( nitBusiness: string, transportationPoint: string, transportationDate: string ) {
    let url = `${environment.END_POINT_REPORT}/demanda/${nitBusiness}`;
    url = url + `?puntoTransporte=${transportationPoint}&fechaTransporte=${transportationDate}`;
    return this._HTTP.get<any>(url)
    .pipe(
      map(
        (response: ResponseDemand) => response.data
      )
    );
  }

}
