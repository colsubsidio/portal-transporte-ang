import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/app/configs/environment';
import { IgeneralResponseTwo } from '../../models/igeneral-response-two';
import { IinfoReservation } from '../../models/iinfo-reservation';
import { RedemptionPetition } from '../../models/redemption-petition';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private _HTTP: HttpService) { }

  public getInfoReservation(type: string, code: string):Observable<IgeneralResponseTwo<IinfoReservation>>{
    const headers = { 'Content-Type': 'application/json' };
    const params = {
      tipo:type,
      codigo:code
    }
    return this._HTTP._get(
      environment.END_POINT,
      '/reservas/boletas',
      params,
      headers
    ).pipe(
      map((response) => {
        return response as IgeneralResponseTwo<IinfoReservation>;
      },err=>{
        return null;
      })
    );
  }

  public redeemPackage(redemptionPetition:RedemptionPetition): Observable<IgeneralResponseTwo<any>>{
    const headers = { 'Content-Type': 'application/json' };
    return this._HTTP._put(
      environment.END_POINT,
      '/reservas',
      {},
      redemptionPetition,
      headers
    ).pipe(
      map((response) => {
        return response as IgeneralResponseTwo<any>;
      })
    );
  }
}
