import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { ERole } from '../models/erole';
import { OauthService } from '../services/oauth/oauth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(private oAuthService: OauthService,
              private router: Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const role: Array<string> = next.data.roles as Array<string>;
      const path = next.routeConfig.path;
      let hasRole = false;
      role.forEach(element =>
        {
            hasRole = (hasRole) ? hasRole : this.oAuthService.hasRole(element);
        });
        
      return hasRole;
  }

}
