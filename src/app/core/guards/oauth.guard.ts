import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { map, take } from 'rxjs/operators';

import { OauthService } from '../services/oauth/oauth.service';

@Injectable({
  providedIn: 'root'
})
export class OauthGuard implements CanActivate {
  constructor(
    private authService: OauthService,
    private router: Router
  ) {}

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {
    const isAuthenticated = this.authService.validateDataToken();
    if(!isAuthenticated){
      this.authService.logout();
    }
    return isAuthenticated;
  }

  

}
