export class DropdownLists {

  public static genders = [
      { name: 'Femenino (F)', id: 1 },
      { name: 'Masculino (M)', id: 2 }
  ];

  public static contracts = [
      { name: 'Fijo', id: '01' },
      { name: 'Indefinido', id: '02' }
  ];

  public static documentPerson = [
      { name: 'Cédula de ciudadanía', id: 'CO1C' },
      { name: 'Tarjeta de identidad', id: 'CO1T' },
      { name: 'Carnet Diplomático', id: 'CO1D' },
      { name: 'Cédula de Extranjería', id: 'CO1E' },
      { name: 'Pasaporte', id: 'CO1P' },
      { name: 'Permiso Especial de Permanencia', id: 'CO1V' }
  ];

  public static civilStatus = [
      { name: 'Soltero/a', id: 1 },
      { name: 'Casado/a', id: 2 },
      { name: 'Separado/a', id: 3 },
      { name: 'Viudo/a', id: 4 },
      { name: 'Unión Libre', id: 5 },
  ];

  public static tratamiento = [
      {name: 'Señor', id: 'sr'},
      {name: 'Señora', id: 'sra'}
  ];

  public static transportQR = [
    { name: 'Escanear código QR', id: 'qr' },
    { name: 'Ingresar código de reserva', id: 'code' },
    { name: 'Ingresar número documento', id: 'numberid' }
  ];

}
