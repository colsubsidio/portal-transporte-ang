import { Injectable } from '@angular/core';

import Swal from 'sweetalert2';
import { ApiService } from '../../services/api/api.service';

@Injectable()
export class DocumentsUtilities {

    constructor(private api: ApiService) { }

    getMessage(message: number) {
        let response: string;
        switch (message) {
            case 3: response = 'Deseas aprobar este documento, después de aprobado no podrá ser modificado!';
            break;
            case 2: response = 'Deseas rechazar este documento, después de rechazado no podrá ser modificado!';
            break;
        }
        return response;
    }

    getMessageResult(messageResult: number) {
        let response: string;
        switch (messageResult) {
            case 3: response = 'El documento se aprobó con éxito.';
            break;
            case 2: response = 'El documento se rechazó con éxito.';
            break;
        }
        return response;
    }

    setSwal(id: string, state: number) {
        const objSwal: object = {
          title: '¿Estás seguro?',
          text: this.getMessage(state),
          inputAttributes: {
            autocapitalize: 'off'
          },
          confirmButtonText: 'Sí',
          cancelButtonText: 'No',
          showCancelButton: true,
          showLoaderOnConfirm: true,
          preConfirm: async ( reasonRejection: any ) => {
            reasonRejection = (reasonRejection === true) ? '' : reasonRejection;
            const url = `api/v1/preafiliaciones/archivo/${id}`;
            const query = {
              'state': state,
              'commentary': reasonRejection
            };
            try {
              const response = await this.api.put(url, query);
              return response;
            } catch (fail) {
              Swal.showValidationMessage(`Request failed:`);
            }
          },
          allowOutsideClick: () => !Swal.isLoading()
        };
        if (state === 2) {
          objSwal['inputPlaceholder'] = 'Breve descripción de por qué rechazas el documento.';
          objSwal['input'] = 'textarea';
        }
        return objSwal;
    }

    validateNIT(numD: any, tipoId: string) {
      if (tipoId === 'nit') {
        return this.addDV(numD);
      } else {
        return numD;
      }
    }

    addDV(numD: any) {
      numD = numD.split('-')[0];
      const codigoVerificacion = this.calculerDV(numD);
      if ((numD !== '' && numD != null) && !isNaN(codigoVerificacion)) {
        return `${numD}-${codigoVerificacion}`;
      }
    }

    calculerDV(nit: string) {
      let arr: any; let x: any; let y: any; let z: any; let i: any; let cod: any;
      arr = new Array(16);
      x = 0; y = 0; z = nit.length;
      arr[1] = 3; arr[2] = 7; arr[3] = 13;
      arr[4] = 17; arr[5] = 19; arr[6] = 23;
      arr[7] = 29; arr[8] = 37; arr[9] = 41;
      arr[10] = 43; arr[11] = 47; arr[12] = 53;
      arr[13] = 59; arr[14] = 67; arr[15] = 71;
      for (i = 0; i < z; i++) {
        y = (nit.substr(i, 1));
        x += (y * arr[z - i]);
      }
      y = x % 11;
      if (y > 1) { cod = 11 - y; } else { cod = y; }
      return cod;
    }
}
