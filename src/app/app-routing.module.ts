import { QrParkModule } from './modules/qr-park/qr-park.module';
import { QrTransporterModule } from './modules/qr-transporter/qr-transporter.module';
import { ERole } from './core/models/erole';
import { RoleGuard } from './core/guards/role.guard';
import { OauthGuard } from './core/guards/oauth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserModule } from './modules/user/user.module';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import {TransportationAdministratorModule} from './modules/transportation-administrator/transportation-administrator.module';
import { QrLayoutComponent } from './layouts/qr-layout/qr-layout.component';
import {ColsubsidioAdministratorModule} from './modules/colsubsidio-administrator/colsubsidio-administrator.module';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    data: {
      breadcrumb: null
    },
    loadChildren: () => UserModule,
  },
  {
    path: 'colsubsidio-administrador',
    component: MainLayoutComponent,
    loadChildren: () => ColsubsidioAdministratorModule,
    //canActivate: [OauthGuard],
    /*data: {
      breadcrumb: '',
      pathComplete : '',
      roles: [ERole.company],
    },*/
  },
  {
    path: 'administrador-transporte',
    component: MainLayoutComponent,
    loadChildren: () => TransportationAdministratorModule,
    canActivate: [OauthGuard],
    /*data: {
      breadcrumb: '',
      pathComplete : '',
      roles: [ERole.company],
    },*/
  },
  {
    path: 'transportador',
    component: QrLayoutComponent,
    loadChildren: () => QrTransporterModule,
    //canActivate: [OauthGuard],
    /*data: {
      breadcrumb: '',
      pathComplete : '',
      roles: [ERole.company],
    },*/
  },
  {
    path: 'parque',
    component: QrLayoutComponent,
    loadChildren: () => QrParkModule,
    //canActivate: [OauthGuard],
    /*data: {
      breadcrumb: '',
      pathComplete : '',
      roles: [ERole.company],
    },*/
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
