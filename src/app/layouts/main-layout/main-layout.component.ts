import { Observable } from 'rxjs';
import { LocalStorageService } from './../../core/services/local-storage/local-storage.service';
import { Component, HostListener, OnInit } from '@angular/core';
import { MenuService } from 'src/app/core/services/menu/menu.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  ifScreenWidth: boolean = false;
  screenWidth: number;
  menuObs$: Observable<boolean>;
  toggleMenu: boolean;

  constructor(private storage: LocalStorageService,
    private _MENUSERVICE: MenuService,) { }

  ngOnInit() {
    this.calculateIfScreenWidth();
    this.menuObs$ = this._MENUSERVICE.getMenuStatus();
  }

  showMenu(toggle) {
    this.toggleMenu = toggle;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = event.target.innerWidth;
    this.calculateIfScreenWidth();
  }

  calculateIfScreenWidth() {
    this.screenWidth < 768 ? this.ifScreenWidth = true : this.ifScreenWidth = false;
  }

}
