import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Breadcrumb } from 'src/app/core/models/ibreadcrumb';
import { Observable } from 'rxjs';
import { filter, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {

  constructor(private _ACTIVATED: ActivatedRoute,
              private router: Router) { }


  breadcrumbs: Breadcrumb[] = this.buildBreadCrumb(this._ACTIVATED.root);
  breadcrumbsSus$: Observable<Breadcrumb[]>;


  ngOnInit() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        distinctUntilChanged(),
        map(event => this.buildBreadCrumb(this._ACTIVATED.root))
      ).subscribe(data => {

        this.breadcrumbs = data;
        //this._CHANGEDETECTOR.markForCheck();
      });

  }


  buildBreadCrumb(route: ActivatedRoute, url: string = '',
                  breadcrumbs: Array<Breadcrumb> = []) {
    if ((route.routeConfig ? (!!((route.routeConfig.data ? (!!route.routeConfig.data.breadcrumb) : false))) : false)) {
      const label = route.routeConfig ? route.routeConfig.data.breadcrumb : 'Home';

      const path = route.routeConfig ? route.routeConfig.data.pathComplete : '';

      const nextUrl = `${url}${path}/`;
      const breadcrumb = {
        label,
        url: nextUrl,
        havePath : (route.routeConfig && route.routeConfig.data.pathComplete !== '')
      };
      const newBreadcrumbs = [...breadcrumbs, breadcrumb];
      newBreadcrumbs.filter( (item: any, index: any) => {
        if ( item.label === 'Inicio' && index === 1 ) {
          newBreadcrumbs.splice(index, 1);
        }
      });
      if (route.firstChild) {
        return this.buildBreadCrumb(route.firstChild, nextUrl, newBreadcrumbs);
      }
      return newBreadcrumbs;
    } else {
      if (route.firstChild) {
        return this.buildBreadCrumb(route.firstChild, url, breadcrumbs);
      }
    }
  }

}
