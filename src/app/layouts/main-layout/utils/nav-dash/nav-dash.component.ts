import { ChangeDetectionStrategy, Component, HostListener, OnInit } from '@angular/core';
import { Menus } from 'src/app/configs/menus';
import { Observable } from 'rxjs';
import { MenuService } from 'src/app/core/services/menu/menu.service';
import { OauthService } from 'src/app/core/services/oauth/oauth.service';
import { MenuItems, MenuItemsChildren } from 'src/app/core/models/menu-items';
import { element } from 'protractor';

@Component({
  selector: 'app-nav-dash',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './nav-dash.component.html',
  styleUrls: ['./nav-dash.component.scss'],
})
export class NavDashComponent implements OnInit {
  public items: Array<MenuItems>;
  menuObs$: Observable<boolean>;
  constructor(private _MENUSERVICE: MenuService,
              private _OAUTH_SERVICE: OauthService) {}

  public isOpen;
  public showReports = {};
  ifScreenWidth: boolean = false;
  screenWidth: number = screen.width;

  ngOnInit(): void {
    this.filterItemsMenu();
    this.menuObs$ = this._MENUSERVICE.getMenuStatus();
    this.reciveChangeToggle();
    this.fillShowMenu();
    this.calculateIfScreenWidth();
  }

  filterItemsMenu(){
    const itemsMenu = Menus.menuItems;
    this.items = new Array<MenuItems>();

    itemsMenu.forEach(val => {
      let item: MenuItems;
      if (this._OAUTH_SERVICE.hasArrayRole(val.roles)){
        item = Object.create(val) as MenuItems;
        if (val.childrens && val.childrens.length>0){
          item.childrens = new Array<MenuItemsChildren>();
          val.childrens.forEach(val2 => {
            if (this._OAUTH_SERVICE.hasArrayRole(val2.roles)){
              item.childrens.push(val2);
            }
          });
        }
      }
      if (item){
        this.items.push(item);
      }
    });

  }

  reciveChangeToggle() {
    this._MENUSERVICE.changeToggle.subscribe((isOpen) => {
      this.isOpen = isOpen;
      if (this.isOpen === false) {
        this.fillShowMenu();
      }
    });
  }

  toogleMenu() {
    this._MENUSERVICE.toogleMenu(this._MENUSERVICE.toogle);
  }

  showMenu(name: string) {
    if (name !== ''){
      this.showReports[name] = !this.showReports[name];
    }
  }

  fillShowMenu(){
    this.items.forEach(
      res => {
        if (res.nameEvent !== ''){
          this.showReports[res.nameEvent] = false;
        }
      }
      );
  }

  logout(){
    this._OAUTH_SERVICE.logout();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = event.target.innerWidth;
    this.calculateIfScreenWidth();
  }

  calculateIfScreenWidth() {
    this.screenWidth < 768 ? this.ifScreenWidth = true : this.ifScreenWidth = false;
  }
}
