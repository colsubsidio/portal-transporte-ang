import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrLayoutComponent } from './qr-layout.component';

describe('QrLayoutComponent', () => {
  let component: QrLayoutComponent;
  let fixture: ComponentFixture<QrLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
