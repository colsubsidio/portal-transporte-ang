import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterQrComponent } from './footer-qr.component';

describe('FooterQrComponent', () => {
  let component: FooterQrComponent;
  let fixture: ComponentFixture<FooterQrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterQrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterQrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
