import { Component, OnInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';
import { OauthService } from 'src/app/core/services/oauth/oauth.service';
import { MenuService } from 'src/app/core/services/menu/menu.service';

@Component({
  selector: 'app-header-qr',
  templateUrl: './header-qr.component.html',
  styleUrls: ['./header-qr.component.scss'],
})
export class HeaderQrComponent implements OnInit {
  isShown: boolean = false;
  public openMenu: boolean = false;
  @Output() showMenu = new EventEmitter();

  constructor(public _MENUSERVICE: MenuService,
              private _OAUTH_SERVICE:OauthService) {}

  ngOnInit() {}

  toogleMenu() {
    this._MENUSERVICE.toogleMenu(this._MENUSERVICE.toogle);
    this.openMenu = !this.openMenu;
  }

  logout(){
    this._OAUTH_SERVICE.logout();
  }
}
